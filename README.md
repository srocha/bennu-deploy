## Levantar el proyecto

``` bash
# bajar las imagenes
docker-compose pull
# levantar el proyecto
docker stack deploy --compose-file docker-compose.yml bennu
# modificar el archivo /etc/hosts y agregar api.bennu.cl
sudo vim /etc/hosts
# tiene que quedar algo así
127.0.0.1	localhost
127.0.1.1	rochabook	api.bennu.cl
```

Listo, ahora ingresar a http://api.bennu.cl
